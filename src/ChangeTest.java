import org.junit.Assert;
import org.junit.Test;

public class ChangeTest {

    @Test
    public void nSmallerThanZeroDyn() {
        assert Change.makeChangedyn(-1,5) == 0;
    }

    @Test
    public void nEqualToZeroSmallerThanFiveDyn() {
        assert Change.makeChangedyn(0,5) == 1;
    }

    @Test
    public void nBiggerThanFourAndDEqualToOneDyn() {
        assert Change.makeChangedyn(5,1) == 1;
    }

    @Test
    public void nBiggerThanFourAndDEqualToFiveDyn() {
        assert Change.makeChangedyn(5,5) == 2;
    }

    @Test
    public void nBiggerThanFourAndDEqualToTenDyn() {
        assert Change.makeChangedyn(5,10) == 2;
    }

    @Test
    public void nBiggerThanFourAndDEqualToTwentyDyn() {
        assert Change.makeChangedyn(5,20) == 1;
    }

    @Test
    public void nBiggerThanFourAndDEqualToTwentyFiveDyn() {
        assert Change.makeChangedyn(5,25) == 2;
    }

    @Test (expected = IllegalArgumentException.class)
    public void nBiggerThanFourAndDNegativeDyn(){
        Change.makeChangedyn(5,-1);
    }

    //Recursive call tests

    @Test
    public void nSmallerThanZeroRec() {
        assert Change.makeChangerec(-1,5) == 0;
    }

    @Test
    public void nEqualToZeroRec() {
        assert Change.makeChangerec(0,5) == 1;
    }

    @Test
    public void nBiggerThanZeroAndDEqualToOneRec() {
        assert Change.makeChangerec(5,1) == 1;
    }

    @Test
    public void nBiggerThanZeroAndDEqualToFiveRec() {
        assert Change.makeChangerec(5,5) == 2;
    }

    @Test
    public void nBiggerThanZeroAndDEqualToTenRec() {
        assert Change.makeChangerec(5,10) == 2;
    }

    @Test
    public void nBiggerThanZeroAndDEqualToTwentyRec() {
        assert Change.makeChangerec(5,20) == 0;
    }

    @Test
    public void nBiggerThanZeroAndDEqualToTwentyFiveRec() {
        assert Change.makeChangerec(5,25) == 2;
    }
}